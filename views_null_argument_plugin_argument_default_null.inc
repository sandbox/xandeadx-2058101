<?php

class views_null_argument_plugin_argument_default_null extends views_plugin_argument_default {
  function get_argument() {
    $this->view->query->add_tag('views_null_argument');
    return '***IS_NULL***';
  }
}

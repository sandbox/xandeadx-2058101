<?php 

/**
 * Implements hook_views_plugins().
 */
function views_null_argument_views_plugins() {
  return array(
    'module' => 'views_null_argument',
    'argument default' => array(
      'null' => array(
        'title' => 'NULL',
        'handler' => 'views_null_argument_plugin_argument_default_null',
      ),
    ),
  );
}

/**
 * Implements hook_views_query_alter().
 */
function views_null_argument_views_query_alter(&$view, &$query) {
  if (in_array('views_null_argument', $query->tags)) {
    foreach ($query->where as &$where_group) {
      foreach ($where_group['conditions'] as &$condition) {
        if ($condition['value'] === '***IS_NULL***') {
          $condition['value'] = NULL;
          $condition['operator'] = 'IS NULL';
        }
      }
    }
  }
}
